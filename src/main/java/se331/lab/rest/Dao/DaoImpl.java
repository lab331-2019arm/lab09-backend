package se331.lab.rest.Dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;
@Slf4j
@Profile("MyDao")
@Repository
public class DaoImpl implements StudentDao{
    List<Student> students;
    public DaoImpl(){
    this.students = new ArrayList<>();
       this.students.add(Student.builder()
               .id(1l)
               .studentId("Se335")
               .name("Kanna")
               .surname("Kobayashi")
               .gpa(3.59)
               .image("https://media.tenor.com/images/c1bbe07e5608a51ab731caccd0860bb6/tenor.gif")
               .penAmount(15)
               .description("The cuteset loli ever!!!")
               .build());
}

    @Override
    public List<Student> getAllStudent() {

        log.info("My dao is call");
        return this.students;
    }


    @Override
    public Student findById(Long id) {
        return students.get((int) (id-1));
    }

    @Override
    public Student saveStudent(Student student) {
        student.setId((long) students.size());
        students.add((student));

        return student;

    }
}