package se331.lab.rest.Dao;

import se331.lab.rest.entity.Student;

import java.util.List;

public interface StudentDao {
    List<Student> getAllStudent();
    Student findById(Long id);
    Student saveStudent(Student student);
}
